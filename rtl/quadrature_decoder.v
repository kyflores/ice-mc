module quadrature_decoder(
  direction_o,
  count_pulse_o,
  error_o,
  clk,
  enable_i,
  reset_i,
  quadA_i,
  quadB_i,
  quadA,
  quadA_old,
  quadB,
  quadB_old
);
output direction_o, count_pulse_o, error_o, quadA, quadA_old, quadB, quadB_old;
input clk, enable_i, reset_i, quadA_i, quadB_i;
wire clk_i, enable_i, reset_i, quadA_i, quadB_i;
reg direction_o, count_pulse_o, error_o;
reg quadA, quadA_old, quadB, quadB_old;

always @(posedge clk) begin
  if (reset_i) begin
    quadA_old <= 0;
    quadB_old <= 0;
    quadA <= 0;
    quadB <= 0;
    direction_o <= 0;
    count_pulse_o <= 0;
    error_o <= 0;
  end 

  if (enable_i) begin
  
    count_pulse_o <= quadA^quadA_old^quadB^quadB_old;
    error_o <= quadA_i^quadA_old && quadB_i^quadB_old;
    direction_o <= quadA_i & !quadB_i & !quadA_old & !quadB_old
              | quadA_i & quadB_i & quadA_old & !quadB_old
              | !quadA_i & quadB_i & quadA_old & quadB_old
              | !quadA_i & !quadB_i & !quadA_old & quadB_old;

    quadA_old <= quadA;
    quadB_old <= quadB;
    quadA <= quadA_i;
    quadB <= quadB_i;
  end
end

/*
always @(quadA_i or quadB_i) begin
  
end
//This section is Asynchronous
/*
always @(quadA_i) begin
  if ( ( quadA_i && quadB_i ) || ( !quadA_i && !quadB_i ) ) begin
      direction_o = 0;
    end else begin
      direction_o = 1;
    end
  count_pulse_o <= 1;
end

always @(quadB_i) begin
  if ( ( quadB_i && !quadA_i ) || ( !quadB_i && quadA_i ) ) begin
      direction_o = 0;
    end else begin
      direction_o = 1;
    end
  count_pulse_o <= 1;
end

/*
always @(posedge count_pulse_o) begin
  count_o <= count_o + direction_o;
  count_pulse_o <= 0;
end
*/

endmodule