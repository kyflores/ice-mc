// The top level module we will use for flashing the FPGA

module top(hwclk, reset, done, miso, mosi, sck, chip_select,
    led1, led2, led3, led4, led5, led6, led7, led8);

// SYSTEM
input hwclk;
input reset;
output done;
output led1, led2, led3, led4, led5, led6, led7, led8;

// SPI
input miso;
output mosi;
output sck;
output chip_select;

// INTERMODULE
wire start_spi, busy, new_data;
wire[7:0] tx_to_din, dout_to_rx;

spi #(.CLK_DIV(16)) spi_periph(
    .clk(hwclk),
    .rst(reset),
    .miso(miso),
    .mosi(mosi),
    .sck(sck),
    .start(start_spi),
    .data_in(tx_to_din),
    .data_out(dout_to_rx),
    .busy(busy),
    .new_data(new_data)
);

spi_multi_transfer #(.TRANSFER_COUNT(2)) dut( 
    .clk(hwclk),
    .rst(reset),
    .start(start),
    .busy(busy),
    .new_data(new_data),
    .tx_data(tx_to_din),
    .rx_data(dout_to_rx),
    .start_spi(start_spi),
    .done(done),
    .chip_select(chip_select)
);

assign led1 = dout_to_rx[0];
assign led2 = dout_to_rx[1];
assign led3 = dout_to_rx[2];
assign led4 = dout_to_rx[3];
assign led5 = dout_to_rx[4];
assign led6 = dout_to_rx[5];
assign led7 = dout_to_rx[6];
assign led8 = dout_to_rx[7];

endmodule
