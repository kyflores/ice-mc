// The top level module we will use for flashing the FPGA

module top(
	hwclk,
	reset,
	pwm1,
	led1,
	led2,
	led3,
	led4,
	led5,
	led6,
	led7,
	led8,
	quadA,
	quadB,
	index);
input hwclk;
input reset;
output pwm1;
output led1, led2, led3, led4, led5, led6, led7, led8;
input quadA, quadB, index;
// verilator lint_off UNUSED
wire [15:0] dummy_val;
wire direction, count_pulse;
reg [15:0] count, rotations;
// verilator lint_on UNUSED

quadrature_decoder quadDecoder(
  .clk(hwclk),
  .enable_i(1'b1),
  .reset_i(reset),
  .quadA_i(quadA),
  .quadB_i(quadB),
  .index_i(index),
  .count_o(count),
  .rotations_o(rotations),
  .count_pulse_o(count_pulse),
  .direction_o(direction)
);


assign led1 = count[0];
assign led2 = count[1];
assign led3 = count[2];
assign led4 = count[3];
assign led5 = count[4];
assign led6 = count[5];
assign led7 = count[6];
assign led8 = count[7];

endmodule
