module debouncer(
  clk_i,
  reset_i,
  pin_i,
  posedge_o,
  negedge_o,
  debounced_o
);

parameter DEBOUNCE_COUNT = 10;

input wire clk_i, pin_i, reset_i;
output reg posedge_o, negedge_o, debounced_o;

reg last_val;
reg[8:0] count; 

always @(posedge clk_i) begin
  if(reset_i) begin
    posedge_o <= 0;
    negedge_o <= 0;
    debounced_o <= 0;
    last_val <= 0;
    count <= 8'b0;
  end else begin
    if (pin_i != last_val) begin
      // If the pin has changed since we checked...
      last_val <= pin_i;
      count <= 0;
    end else begin
      // If the pin is stable
      count = count + 1;
    end
    if (count == DEBOUNCE_COUNT) begin
      debounced_o <= last_val;
      if(last_val) begin
        posedge_o <= 1;
      end else if(!last_val) begin
        negedge_o <= 1;
      end
    end else begin
      posedge_o <= 0;
      negedge_o <= 0;
    end
  end
end

endmodule
