module cordic_tb();

reg clk;
reg[7:0] xI, yI;
reg[15:0] angle;
wire[7:0] xO, yO;

cordic #(.WIDTH(8), .ANGLE_WIDTH(16), .ATAN_INITIAL_FILE("../mems/atan8.bin")) dut(
    .clk(clk),
    .xI(xI),
    .yI(yI),
    .angle(angle),
    .xO(xO),
    .yO(yO)
);

always #1 clk = !clk;
initial begin
    $dumpfile("test.vcd");
    $dumpvars(0,cordic_tb);
    clk = 1'b0; xI = 8'b01000000; yI = 8'b00000000; angle = 16'b0110010010000111;
    repeat (100) begin
        #2 angle = angle + 16'b0000000000111111;
    end
    $finish;
end

endmodule
