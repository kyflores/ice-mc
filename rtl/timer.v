module timer(
  clk,
  enable_i,
  reset_i,
  compare_i,
  autoreload_i,
  compare_o,
  value_o
);
// The bitness of the counter
parameter C_WIDTH = 8;

input wire clk, enable_i, reset_i;
input wire[C_WIDTH-1:0] autoreload_i, compare_i;
output reg compare_o;
output reg[C_WIDTH-1:0] value_o;


always @(posedge clk) begin
  if (reset_i) begin
      value_o <= 0;
  end else if(enable_i) begin
    if(value_o == autoreload_i) begin
        value_o <= 0;
    end else begin
        value_o <= value_o + 1;
    end
    compare_o <= (value_o > compare_i);
  end
end

endmodule
