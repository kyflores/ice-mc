module adder(
    inA,
    inB,
    cin,
    sum,
    cout
);

input inA;
input inB;
input cin;
output sum;
output cout;

assign {cout,sum} = inA + inB + cin;

endmodule
