// The cordic algorithm
// This will be a pipelined design that processes
// one angle rotation per clock cycle. There is
// one rotation per input bit.

// Rotation angle must be between -pi/2 to pi/2
// Angle is stated in I3Q13 as it must accomodate +- 1.57...
// Output is approx 1.64 times larger than reality due to
// the scale factor, which is sum(1 / sqrt(1 + 2^-i)^2) over
// i in 0->Width. It's 1.6467602581210652 to more exact

module cordic(
    clk,
    xI, // Input X value
    yI, // Input Y Value
    angle, // Rotation angle in radians
    xO,
    yO
);
// Bitness of all relevant buses
parameter WIDTH = 16;
parameter ANGLE_WIDTH = 32;
parameter ATAN_INITIAL_FILE = "../mems/atan16_32.hex";

// Inputs
/* verilator lint_off UNUSED */
input clk;
input signed[WIDTH-1:0] xI, yI;
input signed[ANGLE_WIDTH-1:0] angle;
/* verilator lint_on UNUSED */

// Outputs
output signed[WIDTH-1:0] xO, yO;

// Pipeline
reg signed[WIDTH-1:0] x_pl[WIDTH:0]; // X pipeline stages
reg signed[WIDTH-1:0] y_pl[WIDTH:0]; // Y pipeline stages
reg signed[ANGLE_WIDTH-1:0] a_pl[WIDTH:0]; // Angle pipeline stages 

// atan_lut[i] is the result of atan(x) w/ x = 2^-i in radians
reg[ANGLE_WIDTH-1:0] atan_lut[WIDTH-1:0];
initial begin
    $readmemh(ATAN_INITIAL_FILE, atan_lut);
end

// Result depends on angle > 0 for +/-
// xO = xI -/+ (yI >> i)
// yO = yI +/- (xI >> i)
wire[1:0] quadrant;
assign quadrant = angle[31:30];
integer i;
always @(posedge clk) begin
    // Preprocess angle before entering pipeline.
    case (quadrant)
            2'b00, // Tangent is fine in this quadrants, no reflection
            2'b11: begin
                x_pl[0] <= xI;
                y_pl[0] <= yI;
                a_pl[0] <= angle;
            end
            2'b01: begin
                x_pl[0] <= -yI;
                y_pl[0] <= xI;
                a_pl[0] <= {2'b00, angle[29:0]};
            end
            2'b10: begin
                x_pl[0] <= yI;
                y_pl[0] <= -xI;
                a_pl[0] <= {2'b11, angle[29:0]};
            end
    endcase

    // Generate pipeline stages. Might need to break this up if it synths to something awful.
    // Will probably need an alway @(*) block if so to hold the combinational parts.
    for(i = 0; i < WIDTH; i = i + 1) begin
        x_pl[i+1] <= (a_pl[i] > 0) ? x_pl[i] - (y_pl[i] >>> i) : x_pl[i] + (y_pl[i] >>> i);
        y_pl[i+1] <= (a_pl[i] > 0) ? y_pl[i] + (x_pl[i] >>> i) : y_pl[i] - (x_pl[i] >>> i);
        a_pl[i+1] <= (a_pl[i] > 0) ? a_pl[i] - atan_lut[i] : a_pl[i] + atan_lut[i];
    end

end

// Set the output. That sure looks like it means x[0] doesn't it? But it's xO...utput.
assign xO = x_pl[WIDTH-1];
assign yO = y_pl[WIDTH-1];

endmodule
