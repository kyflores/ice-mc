# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "bento/ubuntu-16.04"

  #
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.customize ['modifyvm', :id, '--usb', 'on']
    # Set passthrough filter for programmer
    vb.customize ['usbfilter', 'add', '0', '--target', :id, '--name', 'FTDI', '--vendorid', '0x0403', '--productid', '0x6010']
    vb.memory = "8192"
    vb.cpus = "1"
  end


  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update -y
    apt-get install -y build-essential clang bison flex libreadline-dev \
     gawk tcl-dev libffi-dev git mercurial graphviz   \
     xdot pkg-config python python3 libftdi-dev verilator gtkwave \
     iverilog perl-doc scons ipython cmake

    cd /opt
    git clone https://github.com/cliffordwolf/icestorm.git icestorm
    cd icestorm
    sudo make
    sudo make install

    cd /opt
    git clone https://github.com/cseed/arachne-pnr.git arachne-pnr
    cd arachne-pnr
    sudo make
    sudo make install

    cd /opt
    git clone https://github.com/cliffordwolf/yosys.git yosys
    cd yosys
    sudo make
    sudo make install

    echo " ACTION==\"add\", ATTR{idVendor}==\"0403\", ATTR{idProduct}==\"6010\", MODE:=\"666\"" >\
      /etc/udev/rules.d/53-lattice-ftdi.rules

  SHELL
end
