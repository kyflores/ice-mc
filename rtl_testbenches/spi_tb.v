module spi_tb();

// TB controlled inputs
reg clk, reset, miso, start;
reg[7:0] d_in;

// Module Outputs
wire mosi, sck, busy, new_data;
wire[7:0] d_out;

spi #(.CLK_DIV(2)) dut(
    .clk(clk),
    .rst(reset),
    .miso(miso),
    .mosi(mosi),
    .sck(sck),
    .start(start),
    .data_in(d_in),
    .data_out(d_out),
    .busy(busy),
    .new_data(new_data)
);

// Clk has period of #2, so sck has period of #4
always #1 clk = !clk;
initial begin
    $dumpfile("spi.vcd");
    $dumpvars(0,spi_tb);
    #1 clk = 0; reset = 1; miso = 0; start = 0; d_in = 8'b0;
    #5 reset = 0; start = 1; d_in = 8'b11110000; miso = 1;
    repeat (4) begin
        #8 miso = 1; start = 0;
        $display("BUSY: %b", busy);
    end
    repeat (4) begin
        #8 miso = 0;
        $display("BUSY: %b", busy);
    end
    #10
    $display("GOT: %b\n", d_out);
    $finish;
  
end

endmodule
