module spi_multi_transfer_tb();

// Simulation controls
reg clk, reset, miso, start;

// Simulation read variables
wire done, mosi, sck, chip_select;

// Intermodule Connections
wire start_spi, busy, new_data;
wire[7:0] tx_to_din, dout_to_rx;

spi #(.CLK_DIV(2)) spi_periph(
    .clk(clk),
    .rst(reset),
    .miso(miso),
    .mosi(mosi),
    .sck(sck),
    .start(start_spi),
    .data_in(tx_to_din),
    .data_out(dout_to_rx),
    .busy(busy),
    .new_data(new_data)
);

spi_multi_transfer #(.TRANSFER_COUNT(2)) dut( 
    .clk(clk),
    .rst(reset),
    .start(start),
    .busy(busy),
    .new_data(new_data),
    .tx_data(tx_to_din),
    .rx_data(dout_to_rx),
    .start_spi(start_spi),
    .done(done),
    .chip_select(chip_select)
);

always #1 clk = !clk;
initial begin
    $dumpfile("spi_multi_transfer.vcd");
    $dumpvars(0,spi_multi_transfer_tb);
    clk = 0; reset = 1; miso = 0; start = 0;
    #5 reset = 0; start = 1;
    #10 start = 0;
    #300
    $finish;
end

endmodule
