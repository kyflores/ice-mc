module commutation_tb();

reg clk, enable, reset, advance, direction, break, align;
integer i;
wire [3:0] state;


commutation motor1(
    .clk(clk),
    .enable_i(enable),
    .reset_i(reset),
    .advance_i(advance),
    .direction_i(direction),
    .break_i(break),
    .align_i(align),
    .state_o(state)
);

always #1 clk = !clk;
initial begin
    $dumpfile("commutation.vcd");
    $dumpvars(0,commutation_tb);
    clk = 1'b0; reset = 1'b1;
    #5
    enable = 1'b1; reset = 1'b0; advance = 1'b0; direction = 1'b1; break = 1'b0; align = 1'b0;
    #5
    for (i=0; i < 10; i = i+1) begin
        #1
        advance = 1'b1;
        #1
        advance = 1'b0;
    end
    #1
    break = 1'b1; align = 1'b0;
    #1
    break = 1'b0; align = 1'b1;
    #2
    break = 1'b0; align = 1'b0;
    #5
    $finish;
end

endmodule
