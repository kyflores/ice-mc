module integration_tb();

reg clk, enable, reset, advance, direction, break, align;
integer i;
wire [3:0] state;

reg [15:0] compare, autoreload;
wire compare_out;
wire [15:0] value;

wire pos, neg, db, tim_commutation_out;

commutation motor1(
    .clk(clk),
    .enable_i(enable),
    .reset_i(reset),
    .advance_i(pos),
    .direction_i(direction),
    .break_i(break),
    .align_i(align),
    .state_o(state)
);

timer #(.C_WIDTH(16)) tim_commutation(
    .clk(clk),
    .enable_i(enable),
    .reset_i(reset),
    .compare_i(compare),
    .autoreload_i(autoreload),
    .compare_o(tim_commutation_out),
    .value_o(value)
);

debouncer #(.DEBOUNCE_COUNT(2)) dut(
    .clk_i(clk),
    .reset_i(reset),
    .pin_i(tim_commutation_out),
    .posedge_o(pos),
    .negedge_o(neg),
    .debounced_o(db)
);

always #1 clk = !clk;
initial begin
    $dumpfile("integration.vcd");
    $dumpvars(0,integration_tb);
    clk = 1'b0; enable = 1'b1; reset = 1'b1; compare = 16'h0008; autoreload = 16'h000F;
    #5
    enable = 1'b1; reset = 1'b0; advance = 1'b0; direction = 1'b1; break = 1'b0; align = 1'b0;
    #1000
    $finish;
end

endmodule
