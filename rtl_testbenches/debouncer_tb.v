module debouncer_tb();
reg clk, reset, input_pin;
wire pos, neg, db;

debouncer #(.DEBOUNCE_COUNT(10)) dut(
  .clk_i(clk),
  .reset_i(reset),
  .pin_i(input_pin),
  .posedge_o(pos),
  .negedge_o(neg),
  .debounced_o(db)
);

always #1 clk = !clk;
initial begin
    $dumpfile("test.vcd");
    $dumpvars(0,debouncer_tb);
    clk <= 1'b0; reset <= 1'b1; input_pin <= 1'b0;
    #5
    reset <= 1'b0;
    input_pin <= 1'b1;
    #9
    input_pin <= 1'b0;
    #10
    input_pin <= 1'b1;
    #30
    input_pin <= 1'b0;
    #100
    $finish;
end

endmodule
