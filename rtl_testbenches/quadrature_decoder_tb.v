module quadrature_decoder_tb();

reg clk, enable, reset, quadA, quadB;
wire direction, count_pulse, error, quadAtmp, quadA_oldtmp, quadBtmp, quadB_oldtmp;
//wire signed [7:0] count, rotations;
integer i;

quadrature_decoder quadDecoder(
  .direction_o(direction),
  .count_pulse_o(count_pulse),
  .error_o(error),
  .clk(clk),
  .enable_i(enable),
  .reset_i(reset),
  .quadA_i(quadA),
  .quadB_i(quadB),
  .quadA(quadAtmp),
  .quadA_old(quadA_oldtmp),
  .quadB(quadBtmp),
  .quadB_old(quadB_oldtmp)
);

always #1 clk = !clk;
initial begin
  $dumpfile("quad_test.vcd");
  $dumpvars(0,quadrature_decoder_tb);
  clk <= 1'b0; enable <= 1'b0; reset <= 1'b1; quadA <= 1'b0; quadB <=1'b0;
  #4
  reset = 1'b0; enable <= 1'b1;
  #2
  for (i=0; i < 5; i = i+1) begin
    #2
    quadA <= 1'b1; quadB <=1'b0;
    #2
    quadA <= 1'b1; quadB <=1'b1;
    #2
    quadA <= 1'b0; quadB <=1'b1;
    #2
    quadA <= 1'b0; quadB <=1'b0;
  end
  for (i=0; i < 10; i = i+1) begin
    #3
    quadA <= 1'b0; quadB <=1'b1;
    #3
    quadA <= 1'b1; quadB <=1'b1;
    #3
    quadA <= 1'b1; quadB <=1'b0;
    #3
    quadA <= 1'b0; quadB <=1'b0;
  end
  for (i=0; i < 5; i = i+1) begin
    #4
    quadA <= 1'b1; quadB <=1'b0;
    #4
    quadA <= 1'b1; quadB <=1'b1;
    #4
    quadA <= 1'b0; quadB <=1'b1;
    #4
    quadA <= 1'b0; quadB <=1'b0;
  end
    for (i=0; i < 10; i = i+1) begin
    #5
    quadA <= 1'b0; quadB <=1'b1;
    #5
    quadA <= 1'b1; quadB <=1'b1;
    #5
    quadA <= 1'b1; quadB <=1'b0;
    #5
    quadA <= 1'b0; quadB <=1'b0;
  end
  $finish;
end

endmodule

