module timer_tb();

reg clk, enable, reset;
reg [15:0] compare, autoreload;
wire compare_out;
wire [15:0] value;

timer #(.C_WIDTH(16)) tim(
  .clk(clk),
  .enable_i(enable),
  .reset_i(reset),
  .compare_i(compare),
  .autoreload_i(autoreload),
  .compare_o(compare_out),
  .value_o(value)
);

always #1 clk = !clk;
initial begin
  $dumpfile("test.vcd");
  $dumpvars(0,timer_tb);
  clk <= 1'b0; enable <= 1'b1; reset <= 1'b1; compare <= 16'h0008; autoreload <= 16'h000F;
  #5
  reset = 1'b0;
  #100
  $finish;
end

endmodule
