#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "Vadder.h"
#include <verilated.h>
#include <verilated_vcd_c.h>
#include <iostream>
#include <memory>
#include "simulation_helper.h"

// Import the testing namespace
using namespace ::testing;

// store program args for Verilator
int g_argc;
char **g_argv;

class AdderTest : public Test {
public:
    AdderTest() : trace_(UnitTest::GetInstance()->current_test_info()->name()) {
        Verilated::commandArgs(g_argc, g_argv);
        Verilated::debug(0);
        main_time = 0;
        adder_ = std::unique_ptr<Vadder>(new Vadder);
        adder_->trace(trace_.GetTfpPtr(), 99);
    }
    
    ~AdderTest() {
        adder_->final();
    }

    // Variables //
    vluint64_t main_time;
    std::unique_ptr<Vadder> adder_;
    VcdTracing trace_;
};

TEST_F(AdderTest, zero_plus_zero) {
    // Trivial test. Check out the next one.
    adder_->inA = 0;
    adder_->inB = 0;
    adder_->cin = 0;
    while(main_time < 10) {
        adder_->eval();
        main_time++;
        trace_.Trace(main_time);
    }
    EXPECT_EQ(adder_->sum, 0);
    EXPECT_EQ(adder_->cout, 0);
}

TEST_F(AdderTest, zero_plus_one) {
    trace_.Begin();
    adder_->inA = 0;
    adder_->inB = 0;
    adder_->cin = 0;
    while(main_time < 10) {
        // Set up when values will change first...
        if(main_time == 5 ) {
            adder_->inB = 1;
        }

        // Then call these three functions last.
        adder_->eval();
        trace_.Trace(main_time);
        main_time++;
    }
    EXPECT_EQ(adder_->sum, 1);
    EXPECT_EQ(adder_->cout, 0);
}

TEST_F(AdderTest, one_plus_one_carries_out) {
    adder_->inA = 1;
    adder_->inB = 1;
    adder_->cin = 0;
    while(main_time < 10) {
        adder_->eval();
        main_time++;
        trace_.Trace(main_time);
    }
    EXPECT_EQ(adder_->sum, 0);
    EXPECT_EQ(adder_->cout, 1);
}

TEST_F(AdderTest, one_plus_one_w_cin_carries_out) {
    adder_->inA = 1;
    adder_->inB = 1;
    adder_->cin = 1;
    while(main_time < 10) {
        adder_->eval();
        main_time++;
        trace_.Trace(main_time);
    }
    EXPECT_EQ(adder_->sum, 1);
    EXPECT_EQ(adder_->cout, 1);
}

int main(int argc, char **argv) {
    InitGoogleTest(&argc, argv);
    // Save argc and argv to pass to verilator
    g_argc = argc;
    g_argv = argv;
    return RUN_ALL_TESTS();
}
