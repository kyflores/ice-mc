#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "simulation_helper.h"
#include "Vcordic.h"
#include <verilated.h>
#include <verilated_vcd_c.h>
#include <iostream>
#include <memory>
#include <cmath>
#include <cstdint>

// Import the testing namespace
using namespace ::testing;

// store program args for Verilator
int g_argc;
char **g_argv;

/* DUT
module cordic(
    clk,
    xI, // Input X value
    yI, // Input Y Value
    angle, // Rotation angle in radians
    xO,
    yO
);
*/

class CordicTest : public Test {
public:
    CordicTest() : trace_(UnitTest::GetInstance()->current_test_info()->name()) {
        Verilated::commandArgs(g_argc, g_argv);
        Verilated::debug(0);
        main_time = 0;
        cordic_ = std::unique_ptr<Vcordic>(new Vcordic);
        cordic_->trace(trace_.GetTfpPtr(), 99);
    }

    int ConvertAngle(int deg) {
        float angle = (static_cast<float>(deg)/360.0) * std::pow(2.0,A_WIDTH);
        return static_cast<int>(angle);
    }
    
    ~CordicTest() {
        cordic_->final();
    }

    // Variables //
    vluint64_t main_time;
    std::unique_ptr<Vcordic> cordic_;
    VcdTracing trace_;
    static constexpr uint64_t V_WIDTH = 16;
    static constexpr uint64_t A_WIDTH = 32;
};

TEST_F(CordicTest, PositveRotationAngleDoesSomething) {

    // This magic number is the system gain, which appears because each cordic stage does not
    // factor it out. Since it converges to a certain value always, it's easier to do at the end.
    // This 30000/1.647 should eval close to the maximum value which won't overflow.
    const int16_t x_magnitude = 30000/1.647;
    const float rad_to_deg = 180.0f / 3.1415926f;

    // Assumes 16 bit X,Y, 32 bit Angle
    auto check_angle = [x_magnitude, rad_to_deg] (int32_t angle, int16_t x, int16_t y) {
        float calc_angle = std::atan(static_cast<float>(y) / static_cast<float>(x)) * rad_to_deg;
        float expected_angle = std::atan(std::tan(angle/rad_to_deg))*rad_to_deg;
        EXPECT_TRUE(std::abs(calc_angle)-std::abs(expected_angle) < 1.1f);
    };

    trace_.Begin();
    cordic_->clk = 1;
    cordic_->yI = 0;
    cordic_->xI = x_magnitude;
    cordic_->angle = 0;
    int angle_counter = -180;
    while(main_time < 720) {
        cordic_->clk = (cordic_->clk == 1) ? 0 : 1;
        if(main_time % 2 == 0) {
            angle_counter++;
            cordic_->angle = ConvertAngle(angle_counter);
        }
        cordic_->eval();
        if(angle_counter >= 16) {
            check_angle(angle_counter-16, cordic_->xO, cordic_->yO);
        }
        main_time++;
        trace_.Trace(main_time);
    }
}
