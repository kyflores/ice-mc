import math as m

def get_atan(Power, n):
    """
    Views pi/2 as 2^(n-1), and converts arctan(2^-Power)
    into this format. The format uses n-1 rather than n
    because it assumed to be signed, thus using the nth
    place for representing 2's complement.
    """
    angle = m.atan(2 ** (-Power))
    frac = angle/(2*m.pi)
    return hex(int(frac * (1<<n)))

def get_atanh(Power, n):
    angle = m.atanh(2 ** (-Power))
    frac = angle/(2*m.pi)
    return hex(int(frac * (1<<n)))

if __name__ == '__main__':
    WIDTH = 32
    TABLE_SIZE = 16
    word_length = WIDTH/4 # this many hex characters in output

    print("Generating table with",TABLE_SIZE,"entries and",WIDTH,'bit precision.')
    out_filename = '../mems/atan'+str(TABLE_SIZE)+'_'+str(WIDTH)+'.genhex'
    with open(out_filename, mode='w') as out_file:
        for x in range(TABLE_SIZE):
            val = get_atan(x, WIDTH)
            val = val.lstrip('0x')
            prepend_count = word_length - len(val)
            out_string = (int(prepend_count) * '0') + val + '\n'
            out_file.write(out_string)
    
    print("Done.")
    
