# This script is for use by CMake to invoke verilator to generate C++ files.
# verilator --cc [options] [top_level.v]... [opt_c_files.cpp/c/cc/a/o/so]

import os
import sys

def verilate(filename, output_dir):
    print("Generating from " + filename)
    print("Output to " + output_dir)

    cmd = "verilator "
    opts = "--cc --Wall --trace --Mdir " + output_dir + " "

    full_command = cmd + opts + filename
    print("Running " +full_command)
    os.system(full_command)

if __name__ == "__main__":
    # Call with run_verilator <v_file> <output_dir>
    filename = sys.argv[1]
    output_dir = sys.argv[2]
    verilate(filename, output_dir)
