#pragma once

#include <string>
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>

// A thin wrapper for Verilator's trace functionality.
// Gtest will pass in test case names here to populate the name.
// Use ::testing::UnitTest::GetInstance()->current_test_info()->name()
class VcdTracing {
public:
    VcdTracing(std::string test_name): 
        test_name_(test_name), trace_enabled_(false) {
        Verilated::traceEverOn(true);
    }
    ~VcdTracing() {
        if (trace_enabled_) {
            tfp_.close();
        }
    }
    void Begin() { 
        trace_enabled_ = true;
        std::string vcd_name = test_name_ + ".vcd";
        tfp_.open(vcd_name.c_str());
    }
    VerilatedVcdC* GetTfpPtr() { return &tfp_; }
    void Trace(int timestamp) { tfp_.dump(timestamp); }

private:
    VerilatedVcdC tfp_;
    std::string test_name_;
    bool trace_enabled_;

};
